package obs.springboot.project.business.prpm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import obs.springboot.project.business.prpm.service.FSATableProcessor;

/**
 * Created on Dec 08, 2020
 * @author Shalini Gupta
 *
 * Spring Boot Starter project
 * This is the application class for FSA table service
 * 
 */

@SpringBootApplication
public class PrpmApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrpmApplication.class, args);
	}
	
	@Bean
	public FSATableProcessor getTableProcessor() {
		return new FSATableProcessor();
	}

}
