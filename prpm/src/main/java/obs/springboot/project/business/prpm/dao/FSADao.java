package obs.springboot.project.business.prpm.dao;

/**
 * Created on Dec 08, 2020
 * @author Shalini Gupta
 *
 * Spring Boot Starter project
 * This is the FSA DAO class for FSA table service
 * 
 */
public class FSADao {
	
		// public names of code look-up tables as specified by client call
		// to the DaoFactory.createCodeDao()
		public static final String ACTIVITY_WORKLOADED_BY = "ActivityWorkloadedBy";
		public static final String ALC_SITE = "AlcSite";
		public static final String AMC_AMSC_COMBO = "AmcAmscCombination";
		public static final String BOILERPLATE = "Boilerplate";
		public static final String BPN_CODES = "BusinessPartnerNumber";
		public static final String CAGE = "Cage";	
		public static final String CAGE_PART_XREF = "CagePartXRef";
		public static final String CANCEL_AMEND_REASON = "CancelAmendmentReason";
		public static final String COMMODITY_COUNCIL = "CommodityCouncil";
		public static final String COMMON_WORKBASKET_DESC = "CommonWorkbasketDescriptionView";
		public static final String CONTRACT_ITEM_CATEGORY = "ContractItemCategory";
		public static final String CONTRACT_QUALITY_REQUIREMENTS = "ContractQualityRequirements";
		public static final String CQR_RULES = "CqrRulesDescription";
		public static final String CQR_SPECIAL_INSTRUCTIONS = "CqrSpecialInstructions";
		public static final String DELIVERY_EVENT = "DeliveryEvent";
		public static final String DELIVERY_METHOD = "DeliveryMethod";
		public static final String DISTRIBUTION_CODES = "DistributionCode";
		public static final String DOCUMENTATION_TYPE = "DocumentationType";
		public static final String EAR_REASON_CODE = "EarReasonCode";
		public static final String ENGINEERING_DATA_TYPE = "EngineeringDataType";
		public static final String FSC = "Fsc";
		public static final String FSC_PSEUDO_SELECTION = "FscPseudoCodeSelection";
		public static final String FURNISHED_METHODS = "FurnishedMethods";
		public static final String HELP_CODES = "HelpCodes";
		public static final String IMPEDIMENT_ACTION_TAKEN = "ImpedimentActionTaken";
		public static final String IMPEDIMENT_CODES = "ImpedimentCode";
		public static final String INSPECT_ACCEPT = "InspectionAcceptance";
		public static final String ITEM_MARKING = "ItemMarking";
		public static final String LINE_ITEM_TYPE = "LineItemType";
		public static final String MARKING_METHOD = "MarkingMethod";
		public static final String MARKING_TYPE = "MarkingType";
		public static final String MMAC = "Mmac";
		public static final String PROCUREMENT_ACTIVITY = "ProcurementActivity";
		public static final String PROPRIETARY_RIGHTS = "ProprietaryRights";
		public static final String PSEUDO_CODE = "PseudoCode";
		public static final String PURCHASE_INSTR_NUMBER = "PurchaseInstrumentNumber";
		public static final String PURCHASE_LINE_ITEM_PRIORITY = "PurchaseLineItemPriority";
		public static final String PURCHASE_REQ_TYPE = "PurchaseReqType";
		public static final String QUANTITY_UNIT_PACK = "QuantityUnitPack";
		public static final String RED_STATUS = "RedStatus";
		public static final String RED_PRIORITY = "RedPriority";
		public static final String REQUIREMENT_TYPE = "RequirementType";
		public static final String REQUISITION_PRIORITY = "RequisitionPriority";
		public static final String RMC_RMSC_COMBO = "RmcRmscCombination";
		public static final String RNCC = "Rncc";
		public static final String RNVC = "Rnvc";
		public static final String SCREEN_CANCELLATION = "ScreeningCancellationReason";
		public static final String SCREEN_THRESHOLD = "ScreeningThreshold";
		public static final String SCREENING_PRIORITY = "ScreeningPriority";
		public static final String SCREENING_SOURCE_TYPE = "ScreeningSourceType";
		public static final String SDN_SUB_TYPE = "StandardDocumentNumberSubType";
		public static final String SOURCE_OF_SPPLY_AUTH = "SourceOfSupplyAuthorization";
		public static final String TEXT_ACTIVITY_TYPE = "TextActivityType";
		public static final String UID_FAR_CLAUSE = "UidFarClause";
		public static final String UNIT_OF_ISSUE = "UnitOfIssue";
		public static final String UNIT_OF_MEASURE_ALL = "UnitOfMeasure";
		public static final String UNIT_OF_MEASURE_LINEAR = "UnitOfMeasureLinear";
		public static final String UNIT_OF_MEASURE_WEIGHT = "UnitOfMeasureWeight";
		public static final String USER_TYPE = "UserType";
		public static final String VENDOR_TRNSP_INSTRUCTION = "VendorTransportationInstruction";
		public static final String VENDOR_PKG_INSTRUCTION = "VendorPackagingInstruction";
		public static final String PI_FUNDS_CERTIFICATION_APPLICABILITY = "PiFundsCertificationApplicability";
	    public static final String PI_FUNDS_CERTIFICATION_APPLICABILITY_VIEW = "PiFundsCertificationApplicabilityView";
	    public static final String PI_FUNDS_CERTIFICATION_APPLICABILITY_PI_STATEMENT = "PiFundsCertificationApplicabilityPiStatement";
	    public static final String PI_FUNDS_CERTIFICATION_APPLICABLE_PI_VIEW = "PiFundsCertificationApplicablePiView";
		

}
