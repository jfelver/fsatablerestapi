package obs.springboot.project.business.prpm.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import obs.springboot.project.business.prpm.domain.CodeInfo;
import obs.springboot.project.business.prpm.exception.BaseBusinessException;
import obs.springboot.project.business.prpm.service.FSATableProcessor;

/**
 * Created on Dec 08, 2020
 * @author Shalini Gupta
 * 
 * Spring Boot Starter project
 * This is the controller class that defines the REST API's for FSA table service
 * 
 */

@RestController
@RequestMapping("/fsa")
public class FSAController
{
	@Autowired
	private FSATableProcessor fsaService;
	
	   @GetMapping
	   public ResponseEntity<ArrayList<CodeInfo>> getFSATables() throws BaseBusinessException
	   {
		System.out.println("Its the Rest API for get all FSA tables");
		return new ResponseEntity<ArrayList<CodeInfo>>(fsaService.getFSATables(), HttpStatus.OK);
	   }
		
	   
	   @GetMapping("/{code}")
	   public ResponseEntity<ArrayList<CodeInfo>> getCodeInfoByCodeName(@PathVariable("code") String code) throws BaseBusinessException
	   {
		System.out.println("Its the Rest API for get table rows for a given table code.");
        return new ResponseEntity<ArrayList<CodeInfo>>(fsaService.getCodeInfoByCodeName(code), HttpStatus.OK);
	   }
		
}
