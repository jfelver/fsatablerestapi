package obs.springboot.project.business.prpm.domain;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created on Dec 08, 2020
 * @author Shalini Gupta
 *
 * Spring Boot Starter project
 * This is the POJO class that has information for RED STATUS table for FSA table service
 * 
 */

public class RedStatusInfo extends CodeInfo implements Serializable {
	
    @JsonIgnoreProperties(ignoreUnknown = true)
    
	private String redStatCd = null;
    private String descTxt = null;
	private Date lastUpdDate;
	private String lastUpdBy;

	
	public String getRedStatCd() {
		return redStatCd;
	}
	public void setRedStatCd(String redStatCd) {
		this.redStatCd = redStatCd;
	}
	public String getDescTxt() {
		return descTxt;
	}
	public void setDescTxt(String descTxt) {
		this.descTxt = descTxt;
	}
	public Date getLastUpdDate() {
		return lastUpdDate;
	}
	public void setLastUpdDate(Date lastUpdDate) {
		this.lastUpdDate = lastUpdDate;
	}
	public String getLastUpdBy() {
		return lastUpdBy;
	}
	public void setLastUpdBy(String lastUpdBy) {
		this.lastUpdBy = lastUpdBy;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descTxt == null) ? 0 : descTxt.hashCode());
		result = prime * result + ((lastUpdBy == null) ? 0 : lastUpdBy.hashCode());
		result = prime * result + ((lastUpdDate == null) ? 0 : lastUpdDate.hashCode());
		result = prime * result + ((redStatCd == null) ? 0 : redStatCd.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RedStatusInfo other = (RedStatusInfo) obj;
		if (descTxt == null) {
			if (other.descTxt != null)
				return false;
		} else if (!descTxt.equals(other.descTxt))
			return false;
		if (lastUpdBy == null) {
			if (other.lastUpdBy != null)
				return false;
		} else if (!lastUpdBy.equals(other.lastUpdBy))
			return false;
		if (lastUpdDate == null) {
			if (other.lastUpdDate != null)
				return false;
		} else if (!lastUpdDate.equals(other.lastUpdDate))
			return false;
		if (redStatCd == null) {
			if (other.redStatCd != null)
				return false;
		} else if (!redStatCd.equals(other.redStatCd))
			return false;
		return true;
		}
	
	@Override
	public String toString() {
		return "RedStatusInfo [redStatCd=" + redStatCd + ", descTxt=" + descTxt + ", lastUpdDate=" + lastUpdDate
				+ ", lastUpdBy=" + lastUpdBy + "]";
	}
	
		
}
