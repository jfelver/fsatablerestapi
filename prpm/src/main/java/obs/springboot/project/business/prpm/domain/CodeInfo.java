package obs.springboot.project.business.prpm.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created on Dec 08, 2020
 * @author Shalini Gupta
 *
 * Spring Boot Starter project
 * This is the POJO class that has information for codes for FSA table service
 * 
 */

public class CodeInfo implements Serializable
{
	private String code = null;
	private String description = null;
	private int codeTableId = -1;

	public static final int CQR_CODES = 1;
	public static final int CQR_SPECIAL_INSPECTION = 2;
	public static final int METHOD_OF_DELIVERY = 3;
	public static final int RED_PRIORITY_CODE = 4;
	public static final int RED_STATUS_CODE = 5;
	public static final int TYPE_OF_DATA = 6;
	public static final int DISTRIB_CODES = 7;
	public static final int FURNISHED_METHOD_CODES = 8;
	public static final int PROPRIETARY_RIGHTS_CODES = 9;
	public static final int SAW_CANCEL_CODES = 10;
	public static final int SCREENING_SOURCE_TYPES = 11;
	public static final int ITEM_MARKING_CODES = 12;
	public static final int UNIT_OF_ISSUE_CODES = 13;
	public static final int PSEUDO_MAINTENANCE_CODES = 14;
	public static final int TYPE_LINE_ITEM_CODES = 15;
	public static final int SAW_PRIORITY = 16;
	public static final int CRSAW_PRIORITY = 17;
	public static final int PR_LINE_ITEM_PRIORITY = 18;
	public static final int REQUIREMENT_CATEGORY_MAINTENACE = 19;
	public static final int DOCUMENTATION_TYPE = 20;
	public static final int CAGE = 21;
	public static final int FSC = 22;
	public static final int MMAC = 23;
	public static final int UID_FAR_CLAUSE = 24;
	public static final int PROCUR_ACT_CODES = 25;
	//don't use values from 26-30
	public static final int BOILERPLATE_CODES = 31;	
	public static final int SWPM = 31;
	public static final int CONTRACT_ITEM_CATEGORY_CODES = 32;
	public static final int COMMODITY_COUNCIL_CODES = 33;
	public static final int DELIVERY_EVENT_CODES = 34;
	public static final int COMMON_WORKBASKET = 35;
	public static final int IMPEDIMENT_CODES = 36;
	public static final int IMPEDIMENT_ACTION_TAKEN = 37;
	public static final int RNCC = 38;
	public static final int RNVC = 39;
	public static final int UNIT_OF_MEASURE_LINEAR = 40;
	public static final int UNIT_OF_MEASURE_WEIGHT = 41;
	public static final int UNIT_OF_MEASURE_ALL = 42;
	public static final int CAGE_PART_XREF = 43;
	
	public static final int IUID_MARKING_TYPE = 44;
	public static final int IUID_MARKING_METHOD = 45;
	public static final int IUID_DOLLAR_THRESHOLD = 46;
	public static final int QUANTITY_UNIT_PACK = 47;
	public static final int SDN_SUB_TYPE = 48;
	public static final int BPN = 49;
	
	public static final int TEXT_ACTIVITY_CODES = 50;
	public static final int VENDOR_TRNSP_INSTRUCTION = 51;
	public static final int VENDOR_PKG_INSTRUCTION = 52;
	
	public static final int PI_STATEMENTS_CODES = 53;
    public static final int PI_FUNDS_CERTIFICATION_APPLICABILITY = 54;
    public static final int PI_FUNDS_CERTIFICATION_APPLICABILITY_VIEW = 55;
    public static final int PI_FUNDS_CERTIFICATION_APPLICABLE_PI_VIEW = 56;	
	
    
    @JsonIgnoreProperties(ignoreUnknown = true)
    
	/**
	 * Value object accessor.
	 * 
	 * @return    The value for this field
	 */
	public String getCode()
	{
		return code;
	}

	/**
	 * Value object accessor.
	 * 
	 * @return    The value for this field
	 */
	public String getDescription()
	{
		return description;
	}

	/**
	 * Value object mutator.
	 * 
	 * @param string    The value to store for this field
	 */
	public void setCode(String string)
	{
		code = string;
	}

	/**
	 * Value object mutator.
	 * 
	 * @param string    The value to store for this field
	 */
	public void setDescription(String string)
	{
		description = string;
	}

	/**
	 * Value object accessor.
	 * 
	 * @return    The value for this field
	 */
	public int getCodeTableId()
	{
		return codeTableId;
	}

	/**
	 * Value object mutator.
	 * 
	 * @param i    The value to store for this field
	 */
	public void setCodeTableId(int i)
	{
		codeTableId = i;
	}

	
	@Override
	  public String toString() {
	    return "Code{" +
	        "codeId='" + codeTableId + '\'' +
	        ", description=" + codeTableId +
	        '}';
	  }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + codeTableId;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CodeInfo other = (CodeInfo) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (codeTableId != other.codeTableId)
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		return true;
	}

	
}
