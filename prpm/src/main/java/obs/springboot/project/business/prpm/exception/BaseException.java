package obs.springboot.project.business.prpm.exception;

import java.io.Serializable;


public class BaseException extends Exception implements Serializable
{
	
	/**
	 * Default constructor
	 */
	public BaseException()
	{
		super();
	}
	
	/**
	 * Create an exception with an informational message
	 * 
	 * @param message      The message to attach to the exception
	 */	
	public BaseException(String message)
	{
		super(message);
	}
	
	/**
	 * Message and wrapped exception constructor
	 * 
	 * @param message      The message to attach to the exception
	 * @param cause        The exception being wrapped by this exception
	 */
	public BaseException(String message, Throwable cause)
	{
		super(message,cause);
	}
}
